package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class DBManager {

    private static Connection connection;

    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/midterm_db?useUnicode=true&serverTimezone=UTC","root","");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static Users getUser(String student_id){
        Users user = null;
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE student_id=?");
            statement.setString(1,student_id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()){
                user = new Users(
                        resultSet.getInt("id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("second_name"),
                        resultSet.getString("student_id"),
                        resultSet.getString("password")
                );
            }
            statement.close();



        }catch (Exception e){
            e.printStackTrace();
        }

        return user;
    }
    public static ArrayList<Users> getUsers(){
        ArrayList<Users> users =new ArrayList<>();

        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users ");

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                users.add(new Users(
                        resultSet.getInt("id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("second_name"),
                        resultSet.getString("student_id"),
                        resultSet.getString("password")
                ));
            }
            statement.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        return users;
    }
    public static boolean addUser(String firstname,String secondname,String stID,String pass){
        int row = 0;
        try {
            PreparedStatement statement = connection.prepareStatement("insert into users (first_name,second_name,student_id,password) values (?,?,?,?)");
            statement.setString(1, firstname);
            statement.setString(2, secondname);
            statement.setString(3, stID);
            statement.setString(4, pass);

            row = statement.executeUpdate();
            statement.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return row>0;
    }


    public static ArrayList<Course> getCourses(){
        ArrayList<Course> courses =new ArrayList<>();

        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM courses ");

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                courses.add(new Course( resultSet.getInt("id"),
                                resultSet.getString("course_name")));
            }
            statement.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        return courses;
    }
    public static String getCourseId(String name){
        String id = "";

        try {
            PreparedStatement statement = connection.prepareStatement("SELECT id FROM courses where course_name=? ");
            statement.setString(1,name);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                id = resultSet.getString("id");
            }
            statement.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        return id;
    }
    public static void addCourseToStudent(String student_id, String course_id){
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO student_courses(student_id,course_id) VALUES (?,?)");
            statement.setString(1,student_id);
            statement.setString(2,course_id);
            statement.executeUpdate();
            statement.close();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    public static ArrayList<Course> getCoursesOfStudent(String student_id){
        ArrayList<Course> courses =new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement("select * from courses c  where c.id in (select course_id from student_courses where student_id=?)");
            statement.setString(1,student_id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                courses.add(new Course( resultSet.getInt("id"),
                        resultSet.getString("course_name")));

            }
            statement.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return courses;
    }
    public static boolean addCourse(String course){
        int row = 0;
        try {
            PreparedStatement statement = connection.prepareStatement("insert into courses(course_name) values (?)");
            statement.setString(1, course);
            row = statement.executeUpdate();
            statement.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return row>0;
    }




}

package db;

public class Users {
    private int id;
    private String firstName;
    private String secondName;
    private String student_id;
    private String password;

    public Users() {
    }

    public Users(int id, String firstName, String secondName, String student_id, String password) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.student_id = student_id;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getStudent_id() {
        return student_id;
    }

    public String getPassword() {
        return password;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin</title>
    <%@include file="shared/head.jsp" %>
</head>
<body>

<%@include file="shared/navbar_first.jsp" %>
<div class="container">
    <form action="/addstudent" method="post">
        <div class="form-group mb-1">
            <label>Student name: </label>
            <input type="text" required name="firstname" class="form-control" />
        </div>
        <div class="form-group mb-1">
            <label>Student second name: </label>
            <input type="text" required name="secondname" class="form-control" />
        </div>
        <div class="form-group mb-1">
            <label>Student id: </label>
            <input type="text" required name="student_id" class="form-control" />
        </div>
        <div class="form-group mb-2">
            <label>Password: </label>
            <input type="password" required name="password" class="form-control"/>
        </div>
        <div class="form-group">
            <button class="btn btn-success">Add</button>
            <%--                    <input type="submit" value="Login!">--%>
        </div>
    </form>
</div>
</body>
</html>

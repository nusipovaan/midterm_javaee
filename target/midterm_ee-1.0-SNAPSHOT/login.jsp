<%--
  Created by IntelliJ IDEA.
  User: Acer
  Date: 3/15/2021
  Time: 2:39 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Title</title>
    <%@include file="shared/head.jsp" %>

</head>
<body>

    <div class="container pt-5">
        <h2 class="text-center">Welcome to student management service!</h2>
        <p>Please enter your student id and password to enter the system</p>

        <div class="col-sm-6 ">
            <%
                String passError = request.getParameter("passworderror");
                if (passError != null){
            %>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                Incorrect password!
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            <%
                }
            %>
            <%
                String idError = request.getParameter("iderror");
                if (idError != null){
            %>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                Incorrect student id!
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            <%
                }
            %>


            <form action="/auth" method="post">
                <div class="form-group mb-1">
                    <label>Student ID: </label>
                    <input type="text" required name="student_id" class="form-control" />
                </div>
                <div class="form-group mb-2">
                    <label>Password: </label>
                    <input type="password" required name="password" class="form-control"/>
                </div>
                <div class="form-group">
                    <button class="btn btn-success">LOGIN</button>
<%--                    <input type="submit" value="Login!">--%>
                </div>
            </form>
        </div>

    </div>
</body>
</html>
